<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio(){
        return view('halaman.register');
    } //

    public function daftar(Request $request){
        // dd ($request->all());
        $firstname = $request['first_name'];
        $lastname  = $request['last_name'];
        return view('halaman.welcome',compact('firstname','lastname'));
    }
}
