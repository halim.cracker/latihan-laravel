@extends('layout.master')
@section('judul')
    <h1>Buat Account Baru!</h1>
@endsection
@section('content')
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label for="first_name">First Name:</label>
        <br>
        <br>
        <input type="text" placeholder="First name" name="first_name" id="first_name" required>
        <br>
        <br>
        <label for="last_name">Last Name:</label>
        <br>
        <br>
        <input type="text" placeholder="Last name" name='last_name' id="last_name">
        <br>
        <br>
        <label for="gender">Gender:</label>
        <br>
        <br>
        <input type="radio" id="male" name="gender" value="MALE">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="FEMALE">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="OTHER">
        <label for="other">Other</label>
        <br>
        <br>
        <label for="nationality" id="nation" name="nation">Nationality</label>
        <br>
        <br>
        <select name="Nationality">
            <option value="indonesia">Indonesian</option>
            <option value="inggris">Inggris</option>
            <option value="amerika">Amerika</option>
        </select>
        <br>
        <br>
        <label for="spoken">Language Spoken:</label>
        <br>
        <br>
        <input type="checkbox" id="indonesia" name="spoken" value="BAHASA INDONESIA">
        <label for="male">Bahasa Indonesia</label><br>
        <input type="checkbox" id="english" name="spoken" value="ENGLISH">
        <label for="english">English</label><br>
        <input type="checkbox" id="other" name="spoken" value="OTHER">
        <label for="other">Other</label>
        <br>
        <br>
        <label for="bio">Bio</label>
        <br>
        <br>
        <textarea cols="30" rows="7"></textarea>
        <br>
        <input type="submit" name="tombol" value="kirim">
    </form>
    @endsection