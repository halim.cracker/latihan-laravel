<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'IndexController@index');

Route::get('/register', 'AuthController@bio');

Route::post('/welcome', 'AuthController@daftar');

Route::get('/data-table', function(){
    return view('table.data-table');
});

Route::get('/table', function(){
    return view('table.table');
});


// Route::get('/master', function(){
//     return view('layout.master');
// });create
//CRUD CAST
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');
Route::get('/cast', 'CastController@index');
Route::get('/cast/{cast_id}', 'CastController@show');
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');
Route::delete('/cast/{cast_id}', 'CastController@destroy');